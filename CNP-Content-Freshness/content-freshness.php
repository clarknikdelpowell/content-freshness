<?php

/*
	Plugin Name: Content Freshnesss
	Plugin URI: http://clarknikdelpowell.com
	Version: 1.0
	Description: Adds custom content freshness checking.
	Author: Chris Roche
	Author URI: http://clarknikdelpowell.com

	Copyright 2014+ Clark/Nikdel/Powell (email : croche@clarknikdelpowell.com)

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License, version 2 (or later),
	as published by the Free Software Foundation.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

////////////////////////////////////////////////////////////////////////////////
// PLUGIN CONSTANT DEFINITIONS
////////////////////////////////////////////////////////////////////////////////

define('CNPCF_LOCAL',        $_SERVER['SERVER_NAME'] == 'localhost');
define('CNPCF_PATH',         plugin_dir_path(__FILE__));
define('CNPCF_URL',          CNPCF_LOCAL ? plugins_url().'/content-freshness/' : plugin_dir_url(__FILE__));

////////////////////////////////////////////////////////////////////////////////
// ROOT PLUGIN CLASS
////////////////////////////////////////////////////////////////////////////////


final class CNP_Content_Freshness_Core {

	public $loaded = FALSE;

	public static function initialize() {

		// hook activation
		register_activation_hook( __FILE__, array( 'CNP_Content_Freshness_Core', 'activation' ) );


		if ( class_exists('CNP_Core') ) {

			require_once(CNPCF_PATH.'meta-boxes/CNP-Content-Freshness.php');
			require_once(CNPCF_PATH.'dashboard/CNP-Content-Freshness.php');

			static::$loaded = TRUE;

			CNP_Content_Freshness_Meta_Box::initialize();
			CNP_Content_Freshness_Widget::initialize();
		}
	}

	public static function activation() {
		if ( !class_exists('CNP_Core') ) {
			deactivate_plugins( basename( __FILE__ ) );
			trigger_error('CNP Core required for this plugin to work.', E_USER_ERROR);
		}
	}


}

CNP_Content_Freshness_Core::initialize();